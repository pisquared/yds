import os

from sqlalchemy import Column, Integer, Unicode, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

basepath = os.path.dirname(os.path.realpath(__file__))
DB_PATH = os.path.join(basepath, "db.sqlite")

engine = create_engine('sqlite:///{}'.format(DB_PATH), echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()


def new_db_session():
    return Session()


class Song(Base):
    __tablename__ = 'song'

    id = Column(Integer, primary_key=True)
    filename = Column(Unicode)
    title = Column(Unicode)
