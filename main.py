import sys
from uuid import uuid4

import youtube_dl


class MyLogger(object):
    def debug(self, msg):
        pass

    def info(self, msg):
        print(msg)

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


def yd_download(q, nresults=10):
    filename = str(uuid4())
    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
        'outtmpl': '~/Music/{}.%(ext)s'.format(filename),
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        result = ydl.extract_info("ytsearch{}:{}".format(nresults, q), download=False)
        if 'entries' in result:
            if len(result['entries']) == 1:
                video = result['entries'][0]
            else:
                for i, entry in enumerate(result['entries']):
                    duration = "{}:{}".format(entry['duration'] // 60, entry['duration'] % 60) if 'duration' in entry else "0:00"
                    print("{} {} {}".format(i+1, duration, entry['title']))
                while True:
                    choice = input('> ')
                    if not (choice and choice.isnumeric() and 1 <= int(choice) <= nresults):
                        print("Please choose between 1 and {}".format(nresults))
                    else:
                        choice = int(choice) - 1
                        break
                video = result['entries'][choice]
        else:
            # Just a video
            video = result
        import pdb; pdb.set_trace()
        ydl.download([video.get('webpage_url')])


if __name__ == "__main__":
    q = " ".join(sys.argv[1:])
    # q = "best song in the world"
    print("Searching for: {}".format(q))
    yd_download(q)
